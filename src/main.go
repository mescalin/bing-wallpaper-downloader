package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

/*
 +
 拿到很早以前的
 若 1920x1200 不存在则下载 1366x768 的
 其他下载壁纸的方法
*/
const (
	bing_wallpaper_en = "http://www.bing.com/HPImageArchive.aspx?format=xml&idx=0&n=100&mkt=en-US"
	bing_wallpaper_cn = "http://www.bing.com/HPImageArchive.aspx?format=xml&idx=0&n=100"
	bing_url          = "http://www.bing.com/"
)

var (
	dir_path = "./"
)

func test() {
	date := "20131011"
	image_url := "/az/hprichbg/rb/PalaceofFineArts_EN-US8873797380_1366x768.jpg"
	filename := date + "." + image_url[strings.LastIndex(image_url, "/")+1:]
	fmt.Println(filename)
}

func print_error(e error) {
	fmt.Println("error: ", e)
}

func http_get(url string) (resp *http.Response, err error) {
	fmt.Printf("getting %s ...\n", url)
	client := http.Client{
		Transport: &http.Transport{
			Dial: func(network, addr string) (net.Conn, error) {
				conn, err := net.DialTimeout(network, addr, time.Duration(10*time.Second))
				if err != nil {
					return nil, err
				}
				conn.SetDeadline(time.Now().Add(time.Second * 10))
				return conn, nil
			},
			ResponseHeaderTimeout: time.Second * 10,
		},
	}
	return client.Get(url)
}

func get_wallpaper(date string, image_url string) {
	/*
	   date: 20131011
	   image_url: /az/hprichbg/rb/PalaceofFineArts_EN-US8873797380_1366x768.jpg
	   real url: http://www.bing.com/az/hprichbg/rb/PalaceofFineArts_EN-US8873797380_1366x768.jpg
	   1366x768 -> 1920x1200
	*/

	image_url_1920x1200 := strings.Replace(image_url, "1366x768", "1920x1200", -1)
	filename := dir_path + date + "." + image_url_1920x1200[strings.LastIndex(image_url_1920x1200, "/")+1:]
	_, err := os.Stat(filename)
	if err == nil {
		fmt.Printf("%s already existed\n", filename)
		return
	}

	url := bing_url + image_url_1920x1200
	resp, e := http_get(url)
	if e != nil {
		print_error(e)
		return
	}

	if resp.StatusCode != 200 {
		fmt.Printf("failed get: %s, status = %s\n", url, resp.Status)
		fmt.Println("prepare to get 1366x768")

		filename = dir_path + date + "." + image_url[strings.LastIndex(image_url, "/")+1:]
		_, err = os.Stat(filename)
		if err == nil {
			fmt.Printf("%s already existed\n", filename)
			return
		}

		url = bing_url + image_url
		resp, e = http_get(url)
		if e != nil {
			print_error(e)
			return
		}

		if resp.StatusCode != 200 {
			fmt.Printf("failed get: %s, status = %s\n", url, resp.Status)
			return
		}
	}

	fmt.Printf("success get: %s, status = %s\n", url, resp.Status)

	defer resp.Body.Close() /* close http body */
	b, e := ioutil.ReadAll(resp.Body)
	if e != nil {
		print_error(e)
		return
	}

	f, e := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if e != nil {
		print_error(e)
		return
	}

	defer f.Close() /* close file */
	n, e := f.Write(b)
	if e != nil {
		print_error(e)
		return
	}

	if n != len(b) {
		fmt.Printf("only write %d byte, http body size = %d\n", n, len(b))
		return
	}

	fmt.Printf("success write file: %s, file size = %d\n", filename, n)
}

func get_bing(url string) {
	resp, err := http_get(url)
	if err != nil {
		print_error(err)
		return
	}

	if resp.StatusCode != 200 {
		fmt.Printf("failed get: %s, status = %s\n", url, resp.Status)
		return
	}

	fmt.Printf("success get: %s, status = %s\n", url, resp.Status)

	defer resp.Body.Close() /* close http body */
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print_error(err)
		return
	}

	type image struct {
		XMLName   xml.Name `xml:"image"`
		StartDate string   `xml:"startdate"`
		Url       string   `xml:"url"`
	}

	type images struct {
		XMLName xml.Name `xml:"images"`
		Image   []image  `xml:"image"`
	}

	var body images
	err = xml.Unmarshal(b, &body)
	if err != nil {
		print_error(err)
		return
	}

	for _, v := range body.Image {
		get_wallpaper(v.StartDate, v.Url)
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("请输入壁纸保存路径, (默认为当前路径)")
		time.Sleep(2 * time.Second)
	} else {
		dir_path = os.Args[1]
	}
	get_bing(bing_wallpaper_en)
	get_bing(bing_wallpaper_cn)
}
